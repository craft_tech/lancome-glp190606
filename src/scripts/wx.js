function getQueryParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
        results = regex.exec(location.search);
    return results == null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}
if (navigator.userAgent.toLowerCase().indexOf("micromessenger") != -1) {
    if (!getQueryParameter('info')) {
        location.replace("https://campaign.lancome.com.cn/glp190606/LancomeEyes/OAuth?redirect_url=" + window.location.href.split('#')[0]);
    }
}