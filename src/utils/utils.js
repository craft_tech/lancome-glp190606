class checkList {
    constructor() {
        // constructor(phoneNum, codeNum) {
        // this.phoneNum = phoneNum;
        // this.codeNum = codeNum;
    }
    checkName(name) {
        var reg = /^[\u4e00-\u9fa5a-zA-Z]{2,16}$/;
        if (name == '') {
            alert('请输入姓名')
            return;
        }
        if (!reg.test(name)) {
            alert('名字格式不正确')
            return false;
        } else {
            return true;
        }
    }
    checkPhoneNum(phoneNum) {
        if (phoneNum == '') {
            alert('请输入手机号');
            return;
        }
        if (!/^1[0-9][0-9]\d{8}$/.test(phoneNum)) {
            alert('请输入正确的手机号码');
            return false;
        } else {
            return true;
        }
    }
    checkCodeNum(codeNum, maxLen = 6) {
        if (codeNum == '' || codeNum.length != maxLen) {
            alert('请输入验证码');
            return false;
        } else {
            return true;
        }
    }

    //省市联动判断是否取值obj={'选择省份':province}
    checkAppointmentCounter(obj) {
        for (let item in obj) {
            // console.log(item, obj[item])
            if (item == '' || item == obj[item]) {


                alert('请' + obj[item]);
                return false;
            }
        }

        return true;
    }
    checkPrivacy(status) {
        if (!status) {
            alert('请同意隐私条款');
            return false;
        } else {
            return true;
        }
    }
}

var $An = {
    //淡入
    fadeIn(e, {
        frameSpeed = 20,
        callback,
        fadeInOpacityNum
    } = {}) { //ref
        let _this = this;
        let _style = e.currentStyle || getComputedStyle(e)

        // console.log('样式', _style.display)
        //提前return
        if (e.style.fadeOutAn) return;
        if (_style.display == 'block' && _style.opacity == 1) return;

        e.style.fadeInAn = true;
        e.style.opacity ?
            ((e.style.opacity = fadeInOpacityNum.next().value) >= 1 ?

                (e.style.fadeInAn = false, callback && callback.bind(this)()) :
                setTimeout(() => {
                    _this.fadeIn(e, {
                        frameSpeed,
                        callback,
                        fadeInOpacityNum
                    })
                }, frameSpeed)) :
            (fadeInOpacityNum = _this.fadeInOpacity(e.style.opacity, 10),
                e.style.opacity = fadeInOpacityNum.next().value,
                e.style.display = "block",
                _this.fadeIn(e, {
                    frameSpeed,
                    callback,
                    fadeInOpacityNum
                }))
    },
    * fadeInOpacity(val, maxVal) {
        var index = val * 10;
        while (index <= maxVal)
            yield(index++) / 10;
    },
    //淡出,多参数,回调及this绑定
    fadeOut(e, {
        frameSpeed = 20,
        callback
    } = {}) { //ref
        let _this = this;
        let _style = e.currentStyle || getComputedStyle(e)

        if (e.style.fadeInAn) return;
        if (_style.display == 'none') return;

        e.style.fadeOutAn = true;
        e.style.opacity ?
            ((e.style.opacity -= 0.1) <= 0 ?

                (e.style.display = "none", e.style.opacity = "", e.style.fadeOutAn = false, callback && callback
                    .bind(this)()) :
                setTimeout(() => {
                    _this.fadeOut(e, {
                        frameSpeed,
                        callback
                    })
                }, frameSpeed)) :
            (e.style.opacity = 1, _this.fadeOut(e, {
                frameSpeed,
                callback
            }))
    },
}

var $Dom = {
    getStyle(e, style) {
        let _style = e.currentStyle || getComputedStyle(e)
        return _style[style];
    },
}

var $url = {
    getQueryParameter(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
            results = regex.exec(location.search);
        return results == null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    },
    getTime() {
        // Math.round(new Date().getTime().toString() / 1000).toString()
        return Math.floor((new Date()).getTime() / 1000);
    }
}

export default {
    checkList,
    $An,
    $Dom,
    $url,
}