//判断访问终端
const browser = {
    versions: function() {
        const u = navigator.userAgent,
            app = navigator.appVersion;
        return {
            trident: u.indexOf('Trident') > -1, //IE内核
            presto: u.indexOf('Presto') > -1, //opera内核
            webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
            gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
            mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
            ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
            android: u.indexOf('Android') > -1 || u.indexOf('Adr') > -1, //android终端
            iPhone: u.indexOf('iPhone') > -1, //是否为iPhone或者QQHD浏览器
            iPad: u.indexOf('iPad') > -1, //是否iPad
            webApp: u.indexOf('Safari') == -1, //是否web应该程序，没有头部与底部
            weixin: u.indexOf('MicroMessenger') > -1, //是否微信 （2015-01-22新增）
            qq: u.match(/\sQQ/i) == " qq" //是否QQ
        };
    }(),
    language: (navigator.browserLanguage || navigator.language).toLowerCase()
};

const app = {
    isDevtools: (function() {
        if (location.href.indexOf('localhost') != -1 || location.href.indexOf(':80') != -1) {
            return true;
        } else {
            return false;
        }
    })(),
    device: {
        wechat: (navigator.userAgent.toLowerCase().indexOf("micromessenger") != -1) ? true : false,
        weibo: (navigator.userAgent.toLowerCase().indexOf("weibo") != -1) ? true : false,
        android: !/android/i.test(navigator.userAgent) ? false : true,
        // ios: !/android/i.test(navigator.userAgent) ? true : false,
        ios: browser.versions.ios,
        mobile: browser.versions.mobile,
        androidChrome: /chrome/i.test(navigator.userAgent), //判断安卓浏览器
    },
    userInfo: {
        openId: '',
        source: '',
        mobile: '',
    },
    data: {
        apiDomain: "https://campaign.lancome.com.cn/glp190606/",
        wxOauth: "https://campaign.lancome.com.cn/glp190606/LancomeEyes/OAuth?redirecturl=",
        key: "0E145E73-2D15-4F8F-8A44-75E59918D656",
        preventDefault: true,
    }
}
export default app;
//this._GLOBAL