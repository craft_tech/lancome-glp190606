import 'normalize.css'
import './styles/main.css'

import './scripts/rem.js'
import './scripts/main.js'

/**
 * Init Vue
 */
// import Vue from 'vue'
import Vue from 'vue/dist/vue.esm.js' //完整版vue ，目前使用了运行时vue
import App from './app.vue'

/**
 * Init Vuex
 */
// import Vuex from 'vuex'
// Vue.use(Vuex)

/**
 * Init global_var
 */
import globalVariable from './scripts/global_variable.js'
import Utils from './utils/utils.js'

Vue.prototype._GLOBAL = globalVariable;
Vue.prototype.Utils = Utils;

/**
 * Init Axios
 */
import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.use(VueAxios, axios);
//接口地址
axios.defaults.baseURL = Vue.prototype._GLOBAL.data.apiDomain;

/**
 * Init qs for Axios params
 */
import qs from 'qs'
Vue.prototype._GLOBAL.qs = qs;




/**
 * js-MD5
 */
import md5 from 'js-md5';
Vue.prototype.$md5 = md5;


/**
 * 授权
 * 放到html里
 */
// import wxRedirect from './scripts/wx-redirect.js'

var _openid = Vue.prototype.Utils.$url.getQueryParameter('info');
console.log('openid:', _openid)

if (_openid) {
    Vue.prototype._GLOBAL.userInfo.openId = _openid;
} else {
    // if (Vue.prototype._GLOBAL.device.wechat) {
    //     location.replace(Vue.prototype._GLOBAL.data.wxOauth + window.location.href.split('#')[0]);
    // }
}

var _source = Vue.prototype.Utils.$url.getQueryParameter('utm_source');
Vue.prototype._GLOBAL.userInfo.source = _source;


/**
 * 兼容input样式
 */
var originalHeight = document.documentElement.clientHeight || document.body.clientHeight;
window.onresize = function() {
    var resizeHeight = document.documentElement.clientHeight || document.body.clientHeight;
    if (resizeHeight * 1 < originalHeight * 1) { //resizeHeight<originalHeight被挤压
        document.body.classList.add('isUseInput')
    } else {
        document.body.classList.remove('isUseInput')
    }

    // if (Vue.prototype._GLOBAL.device.wechat && Vue.prototype._GLOBAL.device.ios && !_openid) {
    //     document.body.style.height = (window.innerHeight - 45) + 'px';
    // }
};


/**
 * 微信禁止下滑
 */
document.body.addEventListener('touchmove', function(e) {
    if (Vue.prototype._GLOBAL.data.preventDefault) {
        e.preventDefault();
    }
    // }, {
    //     passive: false
})

// function pushHistory() {
//     var state = {
//         title: "",
//         url: "#"
//     };
//     window.history.pushState(state, "", "");
//     // document.body.style.height = Vue.prototype.Utils.$Dom.getStyle(document.body, 'height')
//     document.body.style.height = window.innerHeight + 'px';
// }
// pushHistory();
// window.addEventListener("popstate", function(e) {
//     WeixinJSBridge.invoke('closeWindow', {}, function(res) {});
// }, false);
// alert(window.innerHeight)
// if (Vue.prototype._GLOBAL.device.wechat && Vue.prototype._GLOBAL.device.ios) {
//     document.body.style.height = (window.innerHeight - 45) + 'px';
// }




/**
 * utm_source
 */


/**
 * Init swiper
 */
// import 'swiper/dist/css/swiper.css'
// import VueAwesomeSwiper from 'vue-awesome-swiper'
// Vue.use(VueAwesomeSwiper)



//render函数也可以渲染组件，且template编译也是调用了render函数，
//运行时vue只能通过render函数来渲染组件，components不行，
//但是.vue文件可以使用components（因为使用了vue-loader进行解析）

// var vm = new Vue({
//     el: '#app',
//     render: function(h) {
//         return h(App)
//     }

//     // components: {
//     //     test: {
//     //         template: '<h1>August</h1>'
//     //     }
//     // }
// })



// Vue.prototype._GLOBAL.isDevtools = false;

/**
 * 提示页跳转
 */
const webpackConfig = require('../config/webpack.config.js'); //配置
if (!Vue.prototype._GLOBAL.isDevtools && !Vue.prototype._GLOBAL.device.mobile) {
    location.replace(webpackConfig.publicPath_prod + 'wx.html');
}


if (!Vue.prototype._GLOBAL.isDevtools && !Vue.prototype._GLOBAL.device.wechat) { //不是微信
    if (!Vue.prototype._GLOBAL.device.weibo) { //不是微博
        //手机浏览器app
        if (Vue.prototype._GLOBAL.device.android && !Vue.prototype._GLOBAL.device.androidChrome) { //不支持的安卓浏览器
            location.replace('./wx.html');
        }
        if (Vue.prototype._GLOBAL.device.ios) { //不支持的ios浏览器
            // location.replace('./wx.html');
        }
    } else { //是微博

    }
}
// console.log(Vue.prototype._GLOBAL)
// console.log(navigator.userAgent)

/**
 * vConsole
 */
// import VConsole from 'vconsole/dist/vconsole.min.js'
// if (Vue.prototype._GLOBAL.device.wechat && Vue.prototype._GLOBAL.isDevtools) {
// if (Vue.prototype._GLOBAL.device.mobile && Vue.prototype._GLOBAL.isDevtools) {
// window.vConsole = new VConsole();
// console.log(vConsole)
// }


if (Vue.prototype._GLOBAL.isDevtools) {
    window.app = Vue.prototype._GLOBAL;
}

//额外的实例，保存全局变量
Vue.prototype._GLOBAL.eventBus = new Vue();

/**
 * Init App
 */
var app = new Vue({
    el: '#app',
    template: '<app/>', //template参数 > el 外部HTML
    components: {
        app: App
    },
});

/**
 * IOS不显示域名
 */
window.alert = function(name) {
    var iframe = document.createElement("IFRAME");
    iframe.style.display = "none";
    iframe.setAttribute("src", 'data:text/plain,');
    document.documentElement.appendChild(iframe);
    window.frames[0].window.alert(name);
    iframe.parentNode.removeChild(iframe);
};